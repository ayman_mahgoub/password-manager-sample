package com.passwordmanager.src.orm;

import com.passwordmanager.src.models.Repository;

/**
 * Created by Omar GamalEldeen on 6/22/15.
 */
public class RepositoriesDao extends GenericDao<Repository> {

    private static RepositoriesDao repositoriesDao;

    private RepositoriesDao() {
        super();
    }

    public static RepositoriesDao getInstance() {

        if (repositoriesDao == null)
            repositoriesDao = new RepositoriesDao();
        return repositoriesDao;
    }


}
